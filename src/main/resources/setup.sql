SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
SET default_with_oids = false;

CREATE FUNCTION public.update_lastchange() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.lastchange = now();
  return new;
end;
$$;

CREATE TABLE public.t_person (
    pk integer generated always as identity primary key,
    pseudo_id text NOT NULL unique,
    email text,
    lastchange timestamp without time zone,
    registered boolean DEFAULT false NOT NULL, 
    consent_pia timestamp without time zone,
    consent_ship timestamp without time zone
);

CREATE TRIGGER trg_lastchange 
BEFORE INSERT OR UPDATE ON public.t_person 
FOR EACH ROW 
EXECUTE PROCEDURE public.update_lastchange();

GRANT ALL ON FUNCTION public.update_lastchange() TO interface_email_collector;

GRANT SELECT,INSERT,UPDATE ON TABLE public.t_person TO interface_email_collector;

CREATE VIEW public.v_version AS
SELECT 1 AS db_version;

grant select on public.v_version to interface_email_collector;
